import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import os
import cv2
import time
from PIL import Image, ImageStat
from scipy import ndimage

def normalize(arr, t_min = 0, t_max = 1):
    """Normalises an array so multiple can be plotted on the same axis easily"""
    norm_arr = []
    diff = t_max - t_min
    diff_arr = max(arr) - min(arr)
    for i in arr:
        temp = (((i - min(arr))*diff)/diff_arr) + t_min
        norm_arr.append(temp)
    return norm_arr

def measure_derivs(img):
    '''Attempts to measure the sharpness of an image with a hard edge by differentiating the greyscale
    values in x and y, then taking whichever is higher as the edge sharpness'''
    img = cv2.resize(img, dsize=[0,0], fx = 0.8, fy=0.8)

    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    im_to_diff = np.array(imgray, dtype = np.int16)

    # Differentiate every row / column of the image, then sum across each to give an overall rate of change in x / y 
    diff_x = np.diff(im_to_diff, axis=1)
    deriv_x = np.abs(diff_x)**2
    
    diff_y = np.diff(im_to_diff, axis=0)
    deriv_y = np.abs(diff_y)**2

    deriv_x = np.sum(deriv_x,axis=1)
    deriv_y = np.sum(deriv_y,axis=0)

    try:
        rate_of_change_x = np.max(deriv_x)
    except:
        rate_of_change_x = 0

    try:
        rate_of_change_y = np.max(deriv_y)
    except:
        rate_of_change_y = 0

    return max([rate_of_change_x, rate_of_change_y])

def looping_usaf_autofocus(microscope, dz=2000, steps = 20, backlash = 100, plot = False, undershoot = 0):
    '''Runs usaf_autofocus repeatedly, until the peak sharpness is in the central 3/5ths of the range.
    Means that autofocuses starting far from the focused position should still focus.'''
    repeat = True
    while repeat:
        all_stats = usaf_autofocus(microscope, dz, steps, backlash, plot, undershoot)
        heights = all_stats[:,0]
        if microscope.position['z'] - min(heights) < dz / 5 or max(heights) - microscope.position['z'] < dz / 5:
            microscope.move_rel([0,0,-undershoot])
            pass
        else:
            repeat = False
            print(f'Focused at {all_stats[np.argmax(all_stats[:,1])][0]}')

def usaf_autofocus(microscope, dz, steps, backlash = 0, plot=False, undershoot = 0):    
    microscope.move_rel([0,0,-int(dz/2 + backlash)])
    microscope.move_rel([0,0,backlash])
    all_stats = []
    imgs = []
    for i in range(steps):
        img = microscope.grab_image_array()

        sharpness_metric = standard_deviation
        # sharpness_metric = measure_derivs
        # sharpness_metric = edge_laplacian

        all_stats.append([microscope.position['z'], sharpness_metric(img)])
        imgs.append(img)
        microscope.move_rel([0,0,int(dz/steps)])
        time.sleep(0.5)

    if plot:
        plt.plot([i[0] for i in all_stats], [i[1] for i in all_stats], '.')
        plt.xlabel('z position (steps)')
        plt.ylabel(f'Sharpness according to {sharpness_metric.__name__}')
        plt.show()

    microscope.move_rel([0,0,-(dz+backlash+undershoot)])
    all_stats = np.array(all_stats)
    microscope.move([microscope.position['x'], microscope.position['y'], all_stats[np.argmax(all_stats[:,1])][0] - undershoot])
    return all_stats

def standard_deviation(img):
    img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_RGB2GRAY))
    # Calculate statistics
    stats = ImageStat.Stat(img)   
    return stats.stddev[0]

def edge_laplacian(img):
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    n: int = 20
    edge: np.ndarray = np.array([[-1] * n + [1] * n])
    image_lap =  float(
        np.sum([np.sum(ndimage.filters.convolve(img, W) ** 2) for W in [edge, edge.T]])
    )
    return image_lap

def unpack(scan_data, start_index = 0):
    jpeg_times = scan_data['jpeg_times']
    jpeg_sizes = scan_data['jpeg_sizes']
    jpeg_sizes_MB = [x / 10**3 for x in jpeg_sizes]
    stage_times = scan_data['stage_times']
    stage_positions = scan_data['stage_positions']
    stage_height = [pos[2] for pos in stage_positions]

    jpeg_heights = np.interp(jpeg_times,stage_times,stage_height)

    return jpeg_heights[start_index:], jpeg_sizes_MB[start_index:]


def z_stack(dz, number_of_images, folder, microscope, backlash = 100):
    start = time.time()

    for i in range(number_of_images):

        height = microscope.position['z']
        
        m = microscope.capture_image_to_disk({
            "temporary": False,
            "use_video_port": False,
            "bayer": True,
            "filename": 'bayer_{0}/{1}'.format(folder, height),
            "tags": [
                    folder.split('/')[0],
                ]
            }
            )
        time.sleep(0.2)
        download_image(microscope,m['id'])
        microscope.move_rel((0,0,dz))
        time.sleep(0.9)
        
    microscope.move_rel((0,0,-(dz*number_of_images+backlash)))
    microscope.move_rel((0,0,backlash))
    print('z stack takes {0}'.format(int(time.time() - start)))

def download_image(microscope, id):
    data = microscope.get_capture_metadata(id)

    file_path = os.path.join('scans', data['path'].removeprefix("/var/openflexure/data/micrographs/"))
    file_path = os.path.split(file_path)[0]
    os.makedirs(file_path, exist_ok=True)
    im = microscope.download_from_id(id, file_path)