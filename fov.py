import numpy as np
import math
import cv2
import matplotlib.pyplot as plt
import skimage
from skimage.feature import peak_local_max
from scipy import ndimage
import usafocus

def get_fov_size(microscope):
    '''Use the image size in pixels and the camera stage mapping matrix to get
    the size of the field of view in motor steps'''
    fov_in_pixels = np.array([832, 624])
    csm = microscope.pull_settings()
    csm_matrix = csm['extensions']['org.openflexure.camera_stage_mapping']['image_to_stage_displacement']

    fov_in_steps = np.matmul(np.flip(fov_in_pixels), csm_matrix)

    return fov_in_pixels, fov_in_steps

def choose_further_angle(angles, edge_imaged):
    '''takes an array of angles 0<=angle<180, and chooses the one furthest away from edge_imaged'''
    diff = np.subtract(angles, edge_imaged)
    diff = np.vstack((diff, 180 - diff))
    angle_to_move = angles[np.argmax(np.min(np.abs(diff), axis = 0))]
    return angle_to_move, np.argmax(np.min(np.abs(diff), axis = 0))

def choose_move(angles, edge_imaged, fov_fraction, img, microscope, fov_in_pixels = np.array([832, 624])):
    '''based on the angles, history of captures and fov, decides where to move next'''

    # if this is a corner
    if len(angles) == 2:
        # if no edge has been imaged yet
        if edge_imaged == None:
            # choose the first angle
            angle = angles[0]

            # move along the chosen line by fov_fraction of the field of view
            x_move = np.cos(np.deg2rad(angle-90)) * fov_fraction * fov_in_pixels[0]
            y_move = - np.sin(np.deg2rad(angle-90)) * fov_fraction * fov_in_pixels[1]

            # if the line is horizontal
            if angle > 45 and angle < 135:
                # want x_move pointing away from vertical edge
                if x_move * (centre_of_mass(img)[1] - img.shape[1]/2) < 0:
                    x_move = x_move*-1
                    y_move = y_move*-1
            
            # if vertical
            else:
                # want y_move pointing towards the area area
                if y_move * (centre_of_mass(img)[0] - img.shape[0]/2) < 0:
                    x_move = x_move*-1
                    y_move = y_move*-1

        # otherwise, one edge has been captured    
        else:

            # choose the edge that hasn't already been imaged
            angle, index = choose_further_angle(angles, edge_imaged)

            x_move = np.cos(np.deg2rad(angle-90)) * fov_fraction * fov_in_pixels[0]
            y_move = - np.sin(np.deg2rad(angle-90)) * fov_fraction * fov_in_pixels[1]



            if angle > 45 and angle < 135:
                print(x_move)
                if x_move * (centre_of_mass(img)[1] - img.shape[1]/2) < 0:
                    x_move = x_move*-1
                    y_move = y_move*-1

            else:
                if y_move * (centre_of_mass(img)[0] - img.shape[0]/2) < 0:
                    x_move = x_move*-1
                    y_move = y_move*-1

        print('Move is {0}, {1}'.format(int(x_move), int(y_move)))
        microscope.move_in_pixels(x = y_move, y = x_move) # TODO talk to Richard about this swap

    # otherwise, there's only one edge on screen
    else:
        # convert array of one angle into just the angle
        angle = int(angles)
        x_move = np.cos(np.deg2rad(angle-90)) * fov_fraction * fov_in_pixels[0]
        y_move = - np.sin(np.deg2rad(angle-90)) * fov_fraction * fov_in_pixels[1]

        # if the current edge is perpendicular to the edge that's been imaged...
        # TODO better test
        if angle - edge_imaged > 45 and angle - edge_imaged > 135:
            # if horizontal, move in negative x
            if angle > 45 and angle < 135:
                if x_move > 0:
                    x_move = x_move*-1
                    y_move = y_move*-1

            # if vertical, move in positive y (in pixel coords)
            else:
                if y_move < 0:
                    x_move = x_move*-1
                    y_move = y_move*-1

        # If still on the same edge that's already been imaged, move in negative y (in pixel coords)
        else:
            if y_move > 0:
                x_move = x_move*-1
                y_move = y_move*-1
            
        print('Move is {0}, {1}'.format(int(x_move), int(y_move)))
        microscope.move_in_pixels(x = y_move, y = x_move)
            

def find_sino(img, print = True):
    '''find the sinogram and edges of the image
    optionally imshow the graphs'''

    # convert to greyscale, then binary mask between 0-100 and 101-255
    # 100 subjectively gives a good estimate of light to dark
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 100, 255, cv2.THRESH_BINARY)
    
    # edges extracts the lines from the binary image, then randon transform returns the sinogram
    edges = cv2.Canny(thresh,100,200)
    sinogram = skimage.transform.radon(edges, theta=None, circle=False, preserve_range=False)
    
    if print:
        fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(8, 4.5))
        ax1.imshow(thresh, cmap = "gray")
        ax2.imshow(edges, cmap='gray')

        ax3.set_title("Radon transform\n(Sinogram)")
        ax3.set_xlabel("Projection angle (deg)")
        ax3.set_ylabel("Projection position (pixels)")
        im = ax3.imshow(sinogram, cmap=plt.cm.Greys_r, extent=(0, 180, -sinogram.shape[0]/2.0, sinogram.shape[0]/2.0), aspect='auto')
        fig.colorbar(im)

        fig.tight_layout()
        plt.show()
    return sinogram, edges

def centre_of_mass(img):
    '''returns the centre of mass of dark areas in an image'''

    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 100, 255, cv2.THRESH_BINARY)
    thresh = (~thresh.astype(bool)).astype(int)
    com = np.asarray(ndimage.measurements.center_of_mass(thresh))

    # returns com, an array of [y,x]
    return com

def test_all_black(img, thresh):
    '''Test if the entire FOV is black (darker than value 'thresh')'''
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return (np.max(img) < thresh)

def test_all_white(img, thresh):
    '''Test if the entire FOV is white (lighter than value 'thresh')'''
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return (np.min(img) > thresh)

def edge_found(xy, img, sino):
    '''if there's only one line in the fov, 
    return the gradient and nearest point on the line 
    to the centre of the image'''

    if len(xy) != 1:
        return 'problem', xy, img
    else:
        m, x1, y1 = line_from_xy(xy, img, sino)
        return m, x1, y1

def corner_found(xy, img, sino):
    '''if there's two lines in the fov, 
    return the gradients and nearest point on the lines
    to the centre of the image
    also returns the coordinates that the lines intercept'''

    if len(xy) != 2:
        return 'problem', 0,0,0,0
    else:
        m, x1, y1 = line_from_xy(xy, img, sino)

        y = [0,0]
        y_inc = [0,0]
        for i in range(len(y)):
            y_inc[i] = y1[i] + m[i] * (0 - x1[i])
        
        xi = (y_inc[0]-y_inc[1]) / (m[1]-m[0])
        yi = m[0] * xi + y_inc[0]

        return m, x1, y1, xi, yi

def line_from_xy(xy, img, sino):
    '''return gradient and coordinates of nearest point'''

    # r needs to be based on centre of image, not corner
    xy[:,0] = xy[:,0] - sino.shape[0] / 2
    r = xy[:, 0]
    angle = xy[:, 1]
    m = np.tan(np.deg2rad(90 - angle))
    x1 = r * np.cos(np.deg2rad(angle)) + img.shape[1] / 2
    y1 = -r * np.sin(np.deg2rad(angle)) + img.shape[0] / 2

    return m, x1, y1

def closed_loop_edge_align(fraction_of_fov, max_loops, max_dist, angle, fx, fy, microscope, verbose = False, fov_in_pixels = np.array([832, 624])):
    '''align the current edge at fraction of fov along the FOV
    max loops is the maximum number of attempts allowed
    max dist is how close before stopping the loop
    fx and fy are the amount to downsample images for speed
    angle is the angle of the line to align to'''

    # if horizontal, choose the height to aim for
    if angle > 45 and angle < 135:
        x_target = fov_in_pixels[0] * fx / 2
        y_target = fraction_of_fov * fov_in_pixels[1] * fy
    
    # if vertical, choose the width
    else:
        x_target = fraction_of_fov * fov_in_pixels[0] * fx
        y_target = fov_in_pixels[1] * fy / 2

    # loop until max_loops have been attempted
    for i in range((max_loops)):
        img = microscope.grab_image_array()
        img = cv2.resize(img, (0,0), fx = fx, fy = fy)
        sino, edges = find_sino(img, print = False)
        xy = peak_local_max(sino, min_distance=10, threshold_abs=6, num_peaks = 2, exclude_border = False)
        angles = xy[:,1]

        # If there's more than one angle, and the difference between the angles is more than 160 (bad test)
        # We assume they are the same peak, due to sinograms being periodic in theta
        if isinstance(angles, np.ndarray) and len(angles) > 1:
            if np.abs(angles[0] - angles[1]) >= 160:
                angles = np.asarray([angles[0]])
                xy = np.asarray([xy[0]])

        # We're supposed to be aligning to one edge - if there's two, something is wrong
            else:
                plt.imshow(img)
                plt.show()
                print(angles)
                return 'problem'
                        
        # Find the point on line closest to the centre of the image
        m, x_nearest, y_nearest = edge_found(xy, img, sino)
        x1, y1 = x_nearest - x_target, y_nearest - y_target

        # If further than max_dist to the centre, move there
        if math.sqrt(x1**2 + y1**2) > max_dist:
            microscope.move_in_pixels(x = int(y1)/fy, y = int(x1)/fx)   # Swapped due to a server [xy] issue - needs fixing
        
        # If closer than max_dist, return success
        else:
            if verbose:
                print('So close we don\'t want to move again. Break')
                plt.imshow(img)
                plt.scatter(x_nearest, y_nearest)
                x = np.linspace(x_nearest-1000,x_nearest+1000,2)
                y = y_nearest + m * (x - x_nearest)
                plt.plot(x, y)
                plt.xlim(0, img.shape[1])
                plt.ylim(0, img.shape[0])
                plt.gca().invert_yaxis()
                plt.show()
            return 'success'

def process_fov(img, edge_imaged, folder, divisions, microscope, dz = 10, stack_height = 13):
    '''Decide what to do with the current FOV
    Takes arguments:
    img - an array of the current FOV
    edge_imaged - False if no edges imaged, otherwise the angle of the edge captured
    folder - a link to the dir to save images
    divisions - how many equally spaced lines across y we want, where the spacing (not the ratio) will be preserved in x
    microscope - the microscope object from openflexure_microscope_client
    dz - the '''

    fov_in_pixels, fov_in_steps = get_fov_size(microscope)

    # Downsizing the image to speed up processing. Changing these would need threshold in find_peaks changing
    fx = 0.1
    fy = 0.1
    img = cv2.resize(img, (0,0), fx = fx, fy = fy)

    # Couple of quick tests that there is black and white areas
    # If only black, move half an FOV top-right
    if test_all_black(img, 120):
        print('All black, moving diagonally to find edge')
        microscope.move_rel((fov_in_steps[0] / 2, fov_in_steps[1] / 2, 0))
        return 'continue', edge_imaged
    # If all-white, we could be anywhere. Abort
    elif test_all_white(img, 120):
        print('All white, aborting')
        return 'exit', edge_imaged

    # If not, we know there's some kind of transition
    else:
        
        # Find sino makes a plot of the angle and position of any lines in the image
        sino, edges = find_sino(img, print = False)
        xy = peak_local_max(sino, min_distance=10, threshold_abs=6, num_peaks = 2, exclude_border = False)
        angles = xy[:,1]

        # If there's more than 1 angle, and they're more than 160 degrees apart,
        # We assume they're the same angle identified twice. Only take the first one
        if isinstance(angles, np.ndarray) and len(angles) > 1:
            if np.abs(angles[0] - angles[1]) >= 160: # need a more elegant test than this?
                angles = np.asarray([angles[0]])
                xy = np.asarray([xy[0]])

        # Now if there's two angles, we know they're from two edges (a corner)
        if isinstance(angles,np.ndarray) and len(angles) > 1:
            print("Corner found with angles {0}".format(angles))
            m, x1, y1, xi, yi = corner_found(xy, img, sino)
            if edge_imaged is None:
                print('Two edges visible, not yet imaged one, moving to one')
                plt.imshow(img)
                plt.show()
                choose_move(angles, edge_imaged, 0.7, img, microscope)
                return 'continue', edge_imaged

            elif xi >= 0 and xi < img.shape[1] and yi >= 0 and yi < img.shape[0]: 
                print('At the corner, one edge done, need to do other')
                plt.imshow(img)
                plt.show()
                print(angles, edge_imaged)
                choose_move(angles, edge_imaged, 0.7, img, microscope)
                return 'continue', edge_imaged
            else:
                print(xi, yi)
                print('Two edges visible, but no corner, moving to corner')
                plt.imshow(img)
                plt.show()
                microscope.move_in_pixels(x = yi - img.shape[1], y = xi - img.shape[0])
                return 'continue', edge_imaged

        elif len(angles) == 1:
            m, x1, y1 = edge_found(xy, img, sino)
            closed_loop_edge_align(1/2, 15, 4, angles, fx, fy, microscope)
            print("Edge found, angle is {0}. Point is {1}, {2}".format(int(angles), int(x1), int(y1)))

            if edge_imaged is None or np.abs(edge_imaged - angles) > 80 and np.abs(edge_imaged - angles) < 100:
                usafocus.looping_usaf_autofocus(microscope, 2000, 20, 100, True)
                result = 'retry'
                attempts = 0
                while result == 'retry':
                    result = test_region(angles, 0.25, fx, fy, microscope)
                    print(result)
                    attempts += 1
                    if attempts > 2 or result == 'not enough space':
                        print('can\'t find an area, aborting')
                        return 'exit', edge_imaged

                distance_in_pixels = fov_in_pixels[1] / (divisions + 1)
                if angles > 45 and angles < 135:
                    fraction = distance_in_pixels / fov_in_pixels[1]
                else: 
                    fraction = distance_in_pixels / fov_in_pixels[0]
                

                # This is how we make sure that the pixel distance in x and y are equal
                # rather than the fraction of the FOV being equal
                # Can probably be written nicer, but the maths works
                offset = (1/2) * ((1 / fraction) - (divisions + 1))

                for i in range(1, divisions + 1):
                    if closed_loop_edge_align((i + offset) * fraction, 15, 4, angles, fx, fy, microscope) == 'problem':
                        print('fov of stack contained corner')
                        return 'exit', 0

                    usafocus.looping_usaf_autofocus(microscope, dz * stack_height, stack_height, 600, True, undershoot = int(0.5 * dz * stack_height))
                    if angles > 45 and angles < 135:
                        prefix = 'horiz/{0}_over_{1}/'.format(int(i), divisions+1)
                    else:
                        prefix = 'vert/{0}_over_{1}/'.format(int(i), divisions+1)
                    sub_folder = folder + '/' + prefix
                    usafocus.z_stack(dz, stack_height, sub_folder, microscope)
                if edge_imaged == None:
                    edge_imaged = angles
                    closed_loop_edge_align(1/2, 15, 4, angles, fx, fy, microscope)
                else:
                    edge_imaged = np.append(edge_imaged, angles)
                    closed_loop_edge_align(1/2, 15, 4, angles, fx, fy, microscope)
                    return 'exit', edge_imaged                
                
            choose_move(angles, edge_imaged, 0.4, img, microscope)
            return 'continue', edge_imaged

        else:
            print('Something doesn\'t make sense')
            return 'exit', edge_imaged

def test_region(angles, distance, fx, fy, microscope, fraction = 0.25, fov_in_pixels = np.array([832, 624])):
    '''test whether the field of view is too close to a corner
    moves along the edge by distance * FOV, and checks if there's a corner.
    if so, moves further away from the corner, and returns retry
    if there's corners on both sides, returns not enough space'''

    dir_allowed = [0,0]
    angle = int(angles)
    x_move = np.cos(np.deg2rad(angle-90)) * distance * fov_in_pixels[0]
    y_move = - np.sin(np.deg2rad(angle-90)) * distance * fov_in_pixels[1]

    microscope.move_in_pixels(x = y_move * fraction, y = x_move * fraction)
    img = microscope.grab_image_array()
    img = cv2.resize(img, (0,0), fx = fx, fy = fy)
    sino, edges = find_sino(img, print = False)
    xy = peak_local_max(sino, min_distance=10, threshold_abs=6, num_peaks = 2, exclude_border = False)
    angles = xy[:,1]

    if isinstance(angles, np.ndarray) and len(angles) > 1:
        if np.abs(angles[0] - angles[1]) >= 160: # need a more elegant test than this
            angles = np.asarray([angles[0]])
            xy = np.asarray([xy[0]])

    if len(angles) > 1:
        dir_allowed[0] = 0
    else:
        dir_allowed[0] = 1

    microscope.move_in_pixels(x = -2 * y_move * fraction, y = -2 * x_move * fraction)
    img = microscope.grab_image_array()
    img = cv2.resize(img, (0,0), fx = fx, fy = fy)
    sino, edges = find_sino(img, print = False)
    xy = peak_local_max(sino, min_distance=10, threshold_abs=6, num_peaks = 2, exclude_border = False)
    angles = xy[:,1]

    if isinstance(angles, np.ndarray) and len(angles) > 1:
        if np.abs(angles[0] - angles[1]) >= 160: # need a more elegant test than this
            angles = np.asarray([angles[0]])
            xy = np.asarray([xy[0]])

    if len(angles) > 1:
        dir_allowed[1] = 0
    else:
        dir_allowed[1] = 1

    microscope.move_in_pixels(x = y_move * fraction, y = x_move * fraction)

    if np.sum(dir_allowed) == 2:
        usafocus.looping_usaf_autofocus(microscope, 1000, 10, 100, True)
        return 'continue'
    if dir_allowed == [1, 0]:
        microscope.move_in_pixels(x = y_move, y = x_move)
        return 'retry'
    if dir_allowed == [0, 1]:
        microscope.move_in_pixels(x = y_move, y = x_move)
        return 'retry'
    else:
        return 'not enough space'
    
