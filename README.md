# auto_mtf

This is the repo for collecting z-stacks to then interpret with the OpenFlexure resolution code found in the USAFCAL branch of [micat](https://gitlab.com/bath_open_instrumentation_group/micat). 

It requires an OpenFlexure Microscope and a large, black-on-light target. The microscope should be calibrated using the calibration wizard to give high-quality images and a camera-to-stage map, allowing closed loop moves.

It's been developed and tested on a USAF 1951 resolution target in brightfield transmission. As is, the code expects two edges to this region: one near-horizontal and one near-vertical. If using something with only one edge within the range of motion (such as a razor blade edge), you'll need to call the relevant functions indepedently and move manually.

## How it works

You can see the overall program flowchart [here](README_imgs/resolution_flowchart.pdf)

Once the microscope is set up facing a dark or edge region, it'll capture an image and test that an edge appears in the current FOV. If no edge is found, and the region is fully dark, the microscope will move towards the top right by a field of view, searching for an edge. If the whole area is light, the program aborts as it is unlikely to be able to locate a useful edge.

If two edges appear in the FOV, we want to only image one edge direction at a time. The sample is moved along one edge until the other is no longer visible. Once only one edge is in the FOV, the system is ready to begin alignment and capturing.

This program allows equally spaced testing of the edge both vertically and horizontally. For example, the user can set ```positions  = 3```, meaning that z-stacks will be captured at 1/4, 1/2 and 3/4 along the (shorter) vertical axis. They will also be captured at 3 points along the horizontal axis - one at the centre of the FOV, and then the same distance away from the centre as the y z-stacks. As the images are wider than tall, this means the x spacing is the same in physical size as y spacing, but a different fraction of the FOV. The edge is aligned using a closed-loop algorithm, allowing precise positioning. More positions means the resolution is more fully characterised, while fewer is quicker to run.

As part of this alignment, the position of other edges is checked - if an unwanted edge is too close to the position of the z stack, it can affect future analysis. If an edge appears too close to the current FOV, the program will shift slightly to try and avoid it, eventually aborting if no suitable region is found.

Autofocusing is performed by tracking the standard deviation of the intensity of the image - sharper images have a wider standard deviation. Users can choose the range to focus over, whether to extend the range if the sharpness appears to peak outside the current range, and whether to undershoot focus ahead of a z-stack. This parameter will need adjusting for various NA objectives, which have higher or lower depths of field.

Once an edge is aligned and focused on, a z-stack is performed. All images in the stack are stored locally, ready for processing in MICAT. If the other edge is also required at this point, it is located and the procedure repeated.

The result of the program should be two top-level folders, one for the vertical edges (giving an estimate of the horizontal Line Spread Function) and vice versa. In each, there will be subfolders for each xy edge position, which will contain a z-stack.

![Image of folder structure](README_imgs/folder_structure.png)
