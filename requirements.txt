matplotlib==3.7.0
numpy==1.24.1
opencv_python==4.7.0.68
openflexure_microscope_client==0.1.8
Pillow==10.0.1
scipy==1.11.2
scikit_image==0.21.0